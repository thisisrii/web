import React from 'react';
import PulseLoader from 'react-spinners/PulseLoader';

const Spinner = ({ loading }) => {
	return (
		<div className="loader" data-testid="test-loader">
			<PulseLoader
				size={15}
				color={'red'}
				loading={loading}
				margin={8}></PulseLoader>
		</div>
	);
};

export default Spinner;