import React from 'react';
import { Route } from 'react-router-dom';

export const PublicRoute = ({
    layout,
    component,
    ...rest
}) => {
    return (
        <Route { ...rest}
        render={(props) => React.createElement(
            layout,
            props,
            React.createElement(component, props))}/>
    );
};