import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthenticationContext from './../../utils/auth/AuthenticationContext';

export const PrivateRoute = ({
    layout: Layout,
    component: Component,
    ...rest
}) => {
    return (
        <AuthenticationContext.Consumer>
            {(auth) => (
                <Route {...rest}
                render={(props) => {
                    if(!auth.isAuthenticated()) {
                        return (
                            <Redirect 
                                to={{
                                    pathname: '/unauthorised', 
                                    state: { from: props.location}, 
                                }}
                            />
                        );
                    }

                    return (
                        <Layout>
                            <Component auth={auth} {...props} />
                        </Layout>
                    )
                }}></Route>
            )}
        </AuthenticationContext.Consumer>
    )
}