import React from 'react';
import { Table } from 'reactstrap';

const DataTable = ({ items }) => {
	const tableBody = items.map((item) => {
		return (
			<tr key={item._id}>
				<td>{item.model}</td>
				<td>{item.make}</td>
				<td>{item.description}</td>
			</tr>
		);
	});

	return (
		<Table responsive hover>
			<thead>
				<tr>
					<th>Make</th>
					<th>Model</th>
					<th>Descrption</th>
				</tr>
			</thead>
			<tbody>{tableBody}</tbody>
		</Table>
	);
};

export default DataTable;
