import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import styles from './ModalForm.module.css';
import AddForm from './AddForm';

const ModalForm = ({ saveItem }) => {
	const [display, setDisplay] = useState(false);

	const toggleModal = () => {
		setDisplay(!display);
	};

	const closeBtn = (
		<button className="close" onClick={toggleModal}>
			&times;
		</button>
	);
	return (
		<div>
			<Button
				color="success"
				onClick={toggleModal}
				className={styles.modalButton}>
				Add New Vehicle
			</Button>
			<Modal isOpen={display} toggle={toggleModal} className={styles.modal}>
				<ModalHeader toggle={toggleModal} close={closeBtn}>
					Add New Vehicle
				</ModalHeader>
				<ModalBody>
					<AddForm saveItem={saveItem} toggleModal={toggleModal}></AddForm>
				</ModalBody>
			</Modal>
		</div>
	);
};

export default ModalForm;
