import React, { useState } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

const AddForm = ({ saveItem, toggleModal }) => {
	const [formData, setFormData] = useState({
		make: '',
		model: '',
		description: '',
	});

	const onChange = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	const submitForm = (e) => {
		e.preventDefault();
		toggleModal();
		saveItem(formData);
	};

	return (
		<Form onSubmit={submitForm}>
			<FormGroup>
				<Label for="make">Make</Label>
				<Input
					type="text"
					name="make"
					id="make"
					onChange={onChange}
					value={formData.make === null ? '' : formData.make}
				/>
			</FormGroup>
			<FormGroup>
				<Label for="model">Model</Label>
				<Input
					type="text"
					name="model"
					id="model"
					onChange={onChange}
					value={formData.model === null ? '' : formData.model}
				/>
			</FormGroup>
			<FormGroup>
				<Label for="description">Description</Label>
				<Input
					type="text"
					name="description"
					id="description"
					onChange={onChange}
					value={formData.description === null ? '' : formData.description}
				/>
			</FormGroup>
			<Button>Submit</Button>
		</Form>
	);
};

export default AddForm;
