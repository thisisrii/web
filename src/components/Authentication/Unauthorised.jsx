import React from 'react';
import AuthenticationContext from '../../utils/auth/AuthenticationContext';
import Spinner from '../Loaders/Spinner';

class Unauthorised extends React.Component {
	static contextType = AuthenticationContext;

	componentDidMount = () => {
		let auth = this.context;
		auth.login();
	};
	render() {
		return <Spinner loading={true}></Spinner>;
	}
}
Unauthorised.contextType = AuthenticationContext;

export default Unauthorised;
