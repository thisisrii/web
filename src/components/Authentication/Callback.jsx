import React from 'react';
import AuthenticationContext from './../../utils/auth/AuthenticationContext';
import Spinner from '../Loaders/Spinner';

class Callback extends React.Component {
	static contextType = AuthenticationContext;

	componentDidMount = () => {
		let auth = this.context;
		auth.handleAuthentication();
	};
	render() {
		return (
			//<h3 className="app-loading" style={{ marginTop: 50, marginLeft: 10 }}>Login Successful. Redirecting to application...</h3>
			<Spinner loading={true}></Spinner>
		);
	}
}

Callback.contextType = AuthenticationContext;

export default Callback;