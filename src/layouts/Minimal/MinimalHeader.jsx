import React from 'react';
import { Link } from 'react-router-dom';

const MinimalHeader = () => {
	return (
		<React.Fragment>
			{/* Navbar */}
			<nav className="bg-gray-900 top-0 w-full md:flex-row md:flex-no-wrap md:justify-start flex items-center p-4">
				<div className="w-full mx-autp items-center flex justify-between md:flex-no-wrap flex-wrap md:px-10 px-4">
					{/* Brand */}
					<Link
						className="hidden md:inline text-center text-red-600 text-xl font-bold"
						to="/dashboard">
						DEMO
						<span className="hidden md:inline text-gray-200 font-thin">
							{' '}
							|{' '}
						</span>
						<span className="hidden md:inline text-white font-normal">
							Demo Application
						</span>
					</Link>
				</div>
			</nav>
			{/* End Navbar */}
		</React.Fragment>
	);
};

export default MinimalHeader;