import React from 'react';
import MinimalHeader from './MinimalHeader';

const MinimalContainer = (props) => {
	return (
		<div>
			<div className="relative">
				<MinimalHeader></MinimalHeader>
				<div>{props.children}</div>
			</div>
		</div>
	);
};

export default MinimalContainer;