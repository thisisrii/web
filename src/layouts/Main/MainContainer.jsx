import React, { memo } from 'react';

const MainContainer = memo((props) => (
    <div>
        <div className="relative md:ml-64">
            {/* Main Header */}
            <div>{props.children}</div>
        </div>
    </div>
))

export default MainContainer;