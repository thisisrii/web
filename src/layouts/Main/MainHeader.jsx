import React from 'react';
import { Link } from 'react-router-dom';

const MainHeader = () => {

	return (
		<React.Fragment>
			{/* Navbar */}
			<nav className="bg-gray-900 top-0 left-0 w-full md:flex-row md:flex-no-wrap md:justify-start flex items-center p-4">
				<div className="w-full mx-autp items-center flex justify-between md:flex-no-wrap flex-wrap md:px-10 px-4">
					{/* Brand */}
					<Link
						className="hidden md:inline text-center text-red-600 text-xl font-bold"
						to="/dashboard">
						DEMO
						<span className="hidden md:inline text-gray-200 font-thin">
							{' '}
							|{' '}
						</span>
						<span className="hidden md:inline text-white font-normal">
							Demo Application
						</span>
					</Link>
					{/* User */}
					<ul className="flex-col md:flex-row list-none items-center hidden md:flex">
						{/* <UserDropdown /> */}
					</ul>
				</div>
			</nav>
			{/* End Navbar */}
		</React.Fragment>
	);
};

export default MainHeader;