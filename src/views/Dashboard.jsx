import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import DataTable from './../components/Tables/DataTable';
import config from './../config';
import ModalForm from './../components/ModalForm/ModalForm';

const Dashboard = () => {
	const history = useHistory();
	const [items, setItems] = useState([]);
	const [isLoading, setIsLoading] = useState(false);

	useEffect(() => {
		getItems();
	}, []);

	async function getItems() {
		setIsLoading(true);
		fetch(`${config.baseUrl}/vehicle`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'auth-token': localStorage.getItem('token'),
			},
		})
			.then((res) => res.json())
			.then((data) => {
				console.log('data', data);
				const hasError = 'error' in data && data.error != null;
				!hasError && setItems(data.data);
			});
	}

	function saveItem(item) {
		setIsLoading(true);
		console.log('saving item', item);
		fetch(`${config.baseUrl}/vehicle`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'auth-token': localStorage.getItem('token'),
			},
			body: JSON.stringify(item),
		})
			.then((res) => res.json())
			.then((data) => {
				console.log('data', data);
				const hasError = 'error' in data && data.error != null;
				!hasError && setItems([...items, item]);
			});
	}

	const logout = () => {
		// eslint-disable-next-line no-restricted-globals
		const toLogout = confirm('Are you sure to logout ?');
		if (toLogout) {
			localStorage.clear();
			history.push('/login');
		}
	};

	return (
		<React.Fragment>
			<nav className="navbar navbar-expand-lg navbar-light bg-light">
				<a className="navbar-brand" href="/">
					Demo Application
				</a>
				<button
					className="navbar-toggler"
					type="button"
					data-toggle="collapse"
					data-target="#navbarText"
					aria-controls="navbarText"
					aria-expanded="false"
					aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarText">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item active">
							<a className="nav-link" href="/dashboard">
								Dashboard <span className="sr-only">(current)</span>
							</a>
						</li>
					</ul>
					<span className="nav-link cursor-pointer" onClick={() => logout()}>
						Logout
					</span>
				</div>
			</nav>
			<div className="px-3">
				<h1>Welcome to the dashboard</h1>
				<Row>
					<Col>
						{items.length >= 0 ? (
							<DataTable items={items}></DataTable>
						) : (
							<p>No Records created by this user.</p>
						)}
					</Col>
				</Row>
				<Row>
					<Col>
						<ModalForm saveItem={saveItem}></ModalForm>
					</Col>
				</Row>
			</div>
		</React.Fragment>
	);
};

export default Dashboard;
